


aps(1) Anya’s password store
════════════════════════════

Dependencies
────────────

  • age(1) for encryption
  • POSIX compliant `/bin/sh'
  • xclip(1) for copying passwords to clipboard (optional)
  • tree(1) for listing passwords directory (optional)
  • tar(1) for encrypted backups (optional)


Non-Features
────────────

  • Delete function (just `rm password')
  • Built-in integration with Git
  • Built-in automatic keypair generation


Software similar to aps(1)
──────────────────────────

  • angou <https://git.13f0.net/angou/>
  • kanako <https://git.kyoko-project.wer.ee/koizumi.aoi/kanako>
  • pash <https://github.com/dylanaraps/pash>

made with <3 ~ puffy
